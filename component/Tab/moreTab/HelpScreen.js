import React, {Component} from 'react';
import {SafeAreaView, Image, View, Text, Button, StyleSheet} from 'react-native';
import colors from '../../config/colors';
import {ButtonWhite} from '../../ButtonWhite';

class Help extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Image
            style={styles.logo}
            source={require('../../img/aerion-logo-white-text.png')}
          />
        <ButtonWhite>FAQ</ButtonWhite>
        <ButtonWhite>Contact Us</ButtonWhite>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.text,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 285,
    height: 56,
  },

});
export default Help;