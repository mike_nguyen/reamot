import {createStackNavigator} from '@react-navigation/stack';
import LoginForm from './LoginForm';
import SignUpForm from './SignupForm';
import WelcomePage from './WelcomePage';
import {NavigationContainer} from '@react-navigation/native';
import React, { Component } from 'react';
const Stack = createStackNavigator();
export default class Navigation extends Component{
    render(){
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="WelcomePage"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="WelcomePage" component={WelcomePage} />
        <Stack.Screen name="LoginForm" component={LoginForm} />
        <Stack.Screen name="SignUpForm" component={SignUpForm} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
}