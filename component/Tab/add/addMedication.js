import React, {Component} from 'react';
import {
  SafeAreaView,
  Text,
  TextInput,
  StyleSheet,
  View,
  Button,
  TouchableOpacity,
  NativeEventEmitter,
  NativeModules,
  Platform,
} from 'react-native';
import colors from '../../config/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {Picker} from '@react-native-community/picker';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment'
import auth from '@react-native-firebase/auth';
import { firebase } from '@react-native-firebase/firestore'
import ReactNativeAN from 'react-native-alarm-notification';
const {RNAlarmNotification} = NativeModules;
const RNEmitter = new NativeEventEmitter(RNAlarmNotification);
const repeatAlarmNotifData = {
  title: 'Alarm',
  message: 'Stand up',
  vibrate: true,
  play_sound: true,
  has_button: true,
  channel: 'wakeup',
  data: {foo: 'bar'},
  loop_sound: true,
  schedule_type: 'repeat',
  repeat_interval: 'daily',
};
class addMedication extends Component {
  state = {
    medName: '',
    strength: 0,
    unit: 'g',
    frequency: 'everyday',
    aDay: 1,
    chosenTime: '',
    dose: 1,
    start: '',
    end: '',
    timeVisible: false,
    startVisible: false,
    endVisible: false,
  };

  createMedication = () => {
    var db = firebase.firestore();
    db.collection('users')
      .doc(auth().currentUser.uid)
      .collection('medications')
      .add({
        medName: this.state.medName,
        strength: this.state.strength,
        unit: this.state.unit,
        frequency: this.state.frequency,
        aDay: this.state.aDay,
        chosenTime: this.state.chosenTime,
        dose: this.state.dose,
        start: new Date(this.state.start),
        end: new Date(this.state.end),
        createdDate: new Date(),
      })
      .then((doc) => {
        this.setAlarm(doc.id);
      });
  };
  calculateAlarmTime = (AlarmTime) => {
    const hour = parseInt(AlarmTime.substring(0, 2));
    const minute = parseInt(AlarmTime.substring(3, 5));
    const second = parseInt(AlarmTime.substring(6, 8));
    const morning_afternoon = AlarmTime.substring(9, 11);
    const now = new Date();
    if (morning_afternoon == 'am') {
      if (hour == 12) {
        now.setHours(hour - 12, minute, second);
      } else {
        now.setHours(hour, minute, second);
      }
    } else if (morning_afternoon == 'pm') {
      if (hour == 12) {
        now.setHours(hour, minute, second);
      } else {
        now.setHours(hour + 12, minute, second);
      }
    }
    if (now <= Date.now()) {
      now.setDate(now.getDate() + 1);
    }
    console.log(moment(now).format());
    return now;
  };
  findIdAN = async (alarm_id, DocId) => {
    const alarm = await ReactNativeAN.getScheduledAlarms();
    let idAN = '';
    for (let i = 0; i < alarm.length; i++) {
      //if (alarm[i].alarmId == alarm_id) {
      idAN = alarm[i].id;
      console.log(`idAN: ${idAN}`);
      firebase
        .firestore()
        .collection('users')
        .doc(auth().currentUser.uid)
        .collection('medications')
        .doc(DocId)
        .update({
          idAN: idAN,
          alarmId: alarm_id,
        });
      //}
    }
    // This is having problem because you cannot set a firestore inside a firestore.
  };
  setAlarm = async (docid) => {
    const alarm = await ReactNativeAN.getScheduledAlarms();
    console.log(alarm);
    firebase
      .firestore()
      .collection('users')
      .doc(auth().currentUser.uid)
      .collection('medications')
      .doc(docid)
      .then((docs) => {
        const alarmID = Math.floor(Math.random() * 10000).toString();
        const AlarmTime = this.calculateAlarmTime(docs.data().chosenTime);
        console.log(moment(AlarmTime).format());
        const details = {
          ...repeatAlarmNotifData,
          // Need to check it.
          fire_date: ReactNativeAN.parseDate(AlarmTime),
          title: doc.data().medName,
          alarm_id: alarmID,
        };
        ReactNativeAN.scheduleAlarm(details);
        this.findIdAN(details.alarm_id, docs.id);
      });
  };

  handleTimePicker = (datetime) => {
    this.setState({
      timeVisible: false,
      chosenTime: moment(datetime).format('h:mm:ss a'),
    });
  };

  handleStartDatePicker = (datetime) => {
    this.setState({
      startVisible: false,
      start: moment(datetime).format('D MMMM YYYY'),
    });
  };

  handleEndDatePicker = (datetime) => {
    this.setState({
      endVisible: false,
      end: moment(datetime).format('D MMMM YYYY'),
    });
  };

  showTimePicker = () => {
    this.setState({timeVisible: true});
  };

  showStartPicker = () => {
    this.setState({startVisible: true});
  };

  showEndPicker = () => {
    this.setState({endVisible: true});
  };

  hidePicker = () => {
    this.setState({
      timeVisible: false,
      startVisible: false,
      endVisible: false,
    });
  };
  componentDidMount() {
    this._subscribeDismiss = RNEmitter.addListener(
      'OnNotificationDismissed',
      (data) => {
        const obj = JSON.parse(data);
        console.log(`notification id: ${obj.id} dismissed`);
      },
    );

    this._subscribeOpen = RNEmitter.addListener(
      'OnNotificationOpened',
      (data) => {
        console.log(data);
        const obj = JSON.parse(data);
        console.log(`app opened by notification: ${obj.id}`);
      },
    );

    // check ios permissions
    if (Platform.OS === 'ios') {
      this.showPermissions();

      ReactNativeAN.requestPermissions({
        alert: true,
        badge: true,
        sound: true,
      }).then(
        (data) => {
          console.log('RnAlarmNotification.requestPermissions', data);
        },
        (data) => {
          console.log('RnAlarmNotification.requestPermissions failed', data);
        },
      );
    }
  }

  componentWillUnmount() {
    this._subscribeDismiss.remove();
    this._subscribeOpen.remove();
  }

  showPermissions = () => {
    ReactNativeAN.checkPermissions((permissions) => {
      console.log(permissions);
    });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.bar}>
          <TextInput
            placeholder="Medication Name"
            style={styles.inputName}
            value={this.state.medName}
            autoCapitalize="words"
            onChangeText={(medName) => this.setState({medName})}
          />
        </View>

        <View style={styles.bar}>
          <Text style={styles.text}>Strength</Text>
          <TextInput
            placeholder="Amount"
            style={styles.input}
            value={this.state.strength}
            onChangeText={(strength) => this.setState({strength})}
          />
          <Picker
            selectedValue={this.state.unit}
            style={{height: 50, width: '20%'}}
            onValueChange={(itemValue, itemIndex) =>
              this.setState({unit: itemValue})
            }>
            <Picker.Item label="g" value="g" />
            <Picker.Item label="IU" value="IU" />
            <Picker.Item label="mcg" value="mcg" />
            <Picker.Item label="mcg/hr" value="mcg/hr" />
            <Picker.Item label="mcg/ml" value="mcg/ml" />
            <Picker.Item label="mEq" value="mEq" />
            <Picker.Item label="mg" value="mg" />
            <Picker.Item label="mg/cm2" value="mg/cm2" />
            <Picker.Item label="mg/g" value="mg/g" />
            <Picker.Item label="mg/ml" value="mg/ml" />
            <Picker.Item label="mL" value="mL" />
            <Picker.Item label="%" value="%" />
          </Picker>
        </View>

        <View style={styles.bar}>
          <Text style={styles.text}>Reminder Times</Text>
          <Picker
            selectedValue={this.state.frequency}
            style={{height: 50, width: '50%'}}
            onValueChange={(itemValue, itemIndex) =>
              this.setState({frequency: itemValue})
            }>
            <Picker.Item label="As Needed" value="asNeeded" />
            <Picker.Item label="Every Day" value="everyday" />
            <Picker.Item label="Specific Days" value="specificDays" />
            <Picker.Item label="Days Interval" value="daysInterval" />
          </Picker>
        </View>

        <View style={styles.bar}>
          <Text style={styles.text}>How many times a day?</Text>
          <Picker
            selectedValue={this.state.aDay}
            style={{height: 50, width: '35%'}}
            onValueChange={(itemValue, itemIndex) =>
              this.setState({aDay: itemValue})
            }>
            <Picker.Item label="Once a Day" value="1" />
            <Picker.Item label="2" value="2" />
            <Picker.Item label="3" value="3" />
            <Picker.Item label="4" value="4" />
            <Picker.Item label="5" value="5" />
            <Picker.Item label="6" value="6" />
            <Picker.Item label="7" value="7" />
            <Picker.Item label="8" value="8" />
            <Picker.Item label="9" value="9" />
            <Picker.Item label="10" value="10" />
          </Picker>
        </View>

        <View style={styles.bar}>
          <Text style={styles.text}>Set Time:</Text>
          <View style={{width: '30%', marginLeft: 10}}>
            <Text>{this.state.chosenTime}</Text>
          </View>
          <Button title="Show Time Picker" onPress={this.showTimePicker} />
          <DateTimePickerModal
            mode="time"
            isVisible={this.state.timeVisible}
            onConfirm={this.handleTimePicker}
            onCancel={this.hidePicker}
            is24Hour={true}
          />
        </View>

        <View style={styles.bar}>
          <Text style={styles.text}>Dose:</Text>
          <Picker
            selectedValue={this.state.dose}
            style={{height: 50, width: '20%'}}
            onValueChange={(itemValue, itemIndex) =>
              this.setState({dose: itemValue})
            }>
            <Picker.Item label="1" value="1" />
            <Picker.Item label="2" value="2" />
            <Picker.Item label="3" value="3" />
            <Picker.Item label="4" value="4" />
            <Picker.Item label="5" value="5" />
            <Picker.Item label="6" value="6" />
            <Picker.Item label="7" value="7" />
            <Picker.Item label="8" value="8" />
            <Picker.Item label="9" value="9" />
            <Picker.Item label="10" value="10" />
          </Picker>
          <Text style={styles.text}>pills</Text>
        </View>

        <View style={styles.bar}>
          <Text style={styles.text}>Start Date:</Text>
          <View style={{width: '30%', marginLeft: 10}}>
            <Text>{this.state.start}</Text>
          </View>
          <Button title="Show Date Picker" onPress={this.showStartPicker} />
          <DateTimePickerModal
            mode="date"
            isVisible={this.state.startVisible}
            onConfirm={this.handleStartDatePicker}
            onCancel={this.hidePicker}
          />
        </View>

        <View style={styles.bar}>
          <Text style={styles.text}>End Date:</Text>
          <View style={{width: '30%', marginLeft: 10}}>
            <Text>{this.state.end}</Text>
          </View>
          <Button title="Show Date Picker" onPress={this.showEndPicker} />
          <DateTimePickerModal
            mode="date"
            isVisible={this.state.endVisible}
            onConfirm={this.handleEndDatePicker}
            onCancel={this.hidePicker}
          />
        </View>
        <Button onPress={this.createMedication} title="Save" />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    alignItems: 'center',
  },
  inputName: {
    marginLeft: 10,
    fontSize: 15,
    width: '70%',
  },
  inputAmount: {
    marginLeft: 10,
    width: '20%',
  },
  bar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderColor: colors.secondary,
    marginTop: 5,
    marginBottom: 5,
    width: '100%',
    height: 50
  },
  text: {
    marginLeft:10,
  }
});
export default addMedication;