import {createStackNavigator} from '@react-navigation/stack';
import * as React from 'react';
import {Component} from 'react';
import {hook, wrap} from 'cavy';
import Home from '../Tab/HomeScreen';

const Stack = createStackNavigator();
class HomeStack extends Component {
  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#96E4DC',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerTitleAlign: 'center',
        }}>
        <Stack.Screen name="Reamot" component={Home} />
      </Stack.Navigator>
    );
  }
}
export default HomeStack;
