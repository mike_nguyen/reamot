import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import colors from './config/colors';

const ButtonOption = ({onPress, children}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.button}>
      <Text style={styles.text}>{children}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    borderBottomWidth: 1,
    borderBottomColor: 'lightgrey',
    padding: 15,
    width: '80%',
    
  },

  text: {
    textAlign: 'left',
    fontSize: 20,
  },
});

export {ButtonOption};
