import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React, {Component} from 'react';
import {hook, wrap} from 'cavy';
import HomeStack from './AppScreenComponent/HomeStack';
import Camera from './AppScreenComponent/Camera';
import EmergencyTab from './Tab/EmergencyScreen';
import NotificationsTab from './Tab/NotificationsScreen';
import More from './Tab/MoreScreen';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
const Tab = createBottomTabNavigator();

class AppScreen extends Component {
  render() {
    return (
      <NavigationContainer>
        <Tab.Navigator
          tabBarOptions={{
            activeTintColor: 'red',
            inactiveTintColor: 'black',
            activeBackgroundColor: '#2699FB',
            inactiveBackgroundColor: '#BCE0FD',
          }}>
          <Tab.Screen
            name="Home"
            component={HomeStack}
            options={{
              tabBarIcon: () => (
                <MaterialIcons
                  name={'home'}
                  style={[{color: 'white'}]}
                  size={25}
                />
              ),
            }}
          />
          <Tab.Screen
            name="Emergency"
            component={EmergencyTab}
            options={{
              tabBarIcon: () => (
                <MaterialIcons
                  name={`call`}
                  style={[{color: 'white'}]}
                  size={25}
                />
              ),
            }}
          />
          <Tab.Screen
            name="Camera"
            component={Camera}
            options={{
              tabBarIcon: () => (
                <MaterialIcons
                  name={'camera-alt'}
                  style={[{color: 'white'}]}
                  size={25}
                />
              ),
            }}
          />
          <Tab.Screen
            name="Notifications"
            component={NotificationsTab}
            options={{
              tabBarIcon: () => (
                <MaterialIcons
                  name={'notifications'}
                  style={[{color: 'white'}]}
                  size={25}
                />
              ),
            }}
          />
          <Tab.Screen
            name="More"
            component={More}
            options={{
              tabBarIcon: () => (
                <MaterialIcons
                  name={'more-horiz'}
                  style={[{color: 'white'}]}
                  size={25}
                />
              ),
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
}
const TestableScene = hook(AppScreen);
export default TestableScene;
