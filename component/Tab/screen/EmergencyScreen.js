import React, {Component} from 'react';
import {SafeAreaView, StyleSheet, FlatList, Text, View, ActivityIndicator} from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import colors from '../../config/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class Emergency extends Component {
  state = {
    loading :true,
    contacts: []
  }

  constructor(props) {
    super(props);
    this.subscriber = 
      firestore()
      .collection('users')
      .doc(auth().currentUser.uid)
      .collection('contact')
      .onSnapshot(docs => {
        let contacts = []
        docs.forEach(doc => {
          contacts.push(doc.data())
        })
        this.setState({ contacts })
      })
  }

  renderList = ({fullName, pNumber}) => {
    return (
      <View style={styles.box}>
        <MaterialIcons name="account-circle" size={24} color="black" />
          {this.state.contacts.map((contact,index) => 
            <View key={index} style={{flex: 1, marginLeft: 8}}>
            <Text style={styles.name}>{contact.fullName}</Text>
            <Text>{contact.pNumber}</Text>
            </View>
          )}
        <MaterialIcons name="chevron-right" size={24} color="black" />
      </View>
    );
  };

  render() {
    return (
    <SafeAreaView style={styles.container}>
      <FlatList style={styles.list} 
        keyExtractor={(item,index) => item.id}
        data={this.state.contacts} 
        renderItem={({item}) => this.renderList(item)}
        showsVerticalScrollIndicator={false}
      />
    </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  list: {
    marginHorizontal: 16,
  },
  box: {
    backgroundColor: "#FFF",
    padding: 8,
    borderRadius: 5,
    marginVertical: 8,
    elevation: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  name: {
    fontSize: 20,
    fontWeight: "500",
  },
});
