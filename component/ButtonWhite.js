import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import colors from './config/colors';

const ButtonWhite = ({onPress, children}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.button}>
      <Text style={styles.text}>{children}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor: 'white',
    padding: 15,
    width: '80%',
    marginTop: 20,
    borderRadius: 5,
  },

  text: {
    textAlign: 'center',
    color: colors.text,
    fontWeight: 'bold',
    fontSize: 20,
  },
});

export {ButtonWhite};
