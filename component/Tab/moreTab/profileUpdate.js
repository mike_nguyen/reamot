import React, {useState, useEffect, Component} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import Ionicons from 'react-native-vector-icons/MaterialIcons';
class profileUpdate extends Component {
  updateUser = async () => {
    await auth().currentUser.updateProfile(update);
  }
  render() {
      var user = auth().currentUser; 
    return (
      <View style={styles.container}>
        <View style={styles.titleBar}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Ionicons name="arrow-back" size={24} color="#52575D" />
          </TouchableOpacity>
        </View>
        <View>
          <View style= {{alignItems:'center'}}>
            <View style={styles.profileImage}>
              <Image
                style={styles.avatar}
                source={
                  user.avatar
                    ? {url: user.photoURL}
                    : require('../../img/default-profile-picture-png-clip-art.png')
                }
                resizeMode="center"
              />
            </View>
            <View styles={styles.dm}>
              <TouchableOpacity>
                <Text style={styles.buttonText}> change Avatar</Text>
              </TouchableOpacity>
            </View>
          </View>
          <TextInput
            placeholder="Name"
            style={styles.input}
            value={user.displayname}
            onChangeText={(val) => this.updateInputVal(val, 'displayName')}
          />
          <TextInput
            placeholder="Email"
            style={styles.input}
            value={user.email}
            Text={(val) => this.updateInputVal(val, 'email')}
          />

          <TouchableOpacity
            style={styles.SignUpContainer}
            onPress={() => this.updateUser()}>
            <Text style={styles.buttonText}>Update</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F1F9FF',
  },
  buttonText: {
    textAlign: 'center',
    color: '#2699FB',
    fontWeight: 'bold',
    fontSize: 20,
  },
  input: {
    paddingRight: 150,
    marginBottom: 20,
    borderRadius: 5,
    fontSize: 15,
    backgroundColor: 'gray',
  },
  titleBar: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 24,
    marginHorizontal: 16,
  },
  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
  },
  logo: {
    justifyContent: 'flex-start',
    width: 66,
    height: 63.92,
  },
  SignUpContainer: {
    borderRadius: 8,
    backgroundColor: '#BCE0FD',
    padding: 15,
    bottom: 3,
  },
  loginText: {
    color: '#2699FB',
    fontWeight: 'bold',
    bottom: 5,
    textAlign: 'center',
  },
  profileImage: {
    width: 300,
    height: 300,
    borderRadius: 100,
    overflow: 'hidden',
  },
  avatar: {
    flex: 1,
    width: undefined,
    height: undefined,
  },
  dm: {
    backgroundColor: '#41444B',
    position: 'absolute',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default profileUpdate;
