/* eslint-disable no-undef */
import 'react-native-gesture-handler';
import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {Component} from 'react';
import Home from './Tab/HomeScreen';
import Emergency from './Tab/EmergencyStack';
import CameraScreen from './Tab/CameraScreen';
import More from './Tab/MoreScreen';
import Notifications from './Tab/NotificationsScreen';
import WelcomePage from './WelcomePage';
import auth from '@react-native-firebase/auth';
import Loading from './Loading';
import {useState, useEffect} from 'react';
import colors from '../component/config/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import firestore, { firebase } from '@react-native-firebase/firestore';
import { enableScreens } from 'react-native-screens';

import {GoogleSignin} from '@react-native-community/google-signin';
enableScreens();

async function bootstrap() {
  await firestore().settings({
    persistence: false, // disable offline persistence
  });
}

class HomeScreen extends Component {
  render() {
    return <Home />;
  }
}

class NotificationScreen extends Component {
  render() {
    return <Notifications />;
  }
}


//function
const Tab = createMaterialBottomTabNavigator();
const Stack = createNativeStackNavigator();

//Home
class HomeTab extends Component {
  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Home" component={HomeScreen} />
      </Stack.Navigator>
    );
  }
}

//Camera
function Camera() {
  return (
    <CameraScreen />
  );
}

//Notifications
class NotificationsTab extends Component {
  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: colors.text,
          },
          headerTintColor: 'white',
          headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: 20,
          },
        }}>
        <Stack.Screen
          name="Notifications"
          component={NotificationScreen}
          options={{
            title: 'Notifications',
          }}
        />
      </Stack.Navigator>
    );
  }
}

//Bottom Navigator
function AppScreen() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen
          name="Home"
          component={HomeTab}
          options={{
            tabBarIcon: () => (
              <MaterialIcons
                name={'home'}
                style={[{color: 'white'}]}
                size={25}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Emergency"
          component={Emergency}
          options={{
            tabBarIcon: () => (
              <MaterialIcons
                name={'call'}
                style={[{color: 'white'}]}
                size={25}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Camera"
          component={Camera}
          options={{
            tabBarIcon: () => (
              <MaterialIcons
                name={'camera-alt'}
                style={[{color: 'white'}]}
                size={25}
              />
            ),
          }}
        />
        <Tab.Screen
          name="Notifications"
          component={NotificationsTab}
          options={{
            tabBarIcon: () => (
              <MaterialIcons
                name={'notifications'}
                style={[{color: 'white'}]}
                size={25}
              />
            ),
          }}
        />
        <Tab.Screen
          name="More"
          component={More}
          options={{
            tabBarIcon: () => (
              <MaterialIcons
                name={'more-horiz'}
                style={[{color: 'white'}]}
                size={25}
              />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}


function SplashScreen() {
  return <Loading />;
}
const App = () => {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  // Handle user state changes
  function onAuthStateChanged(user) {
    firebase.firestore().collection('users');
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    GoogleSignin.configure({
  webClientId:
    '78045108977-atpb9is5g7kuku18kvijkl440eg2rs43.apps.googleusercontent.com',
});
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;

  if (!user) {
    return (
      <View style={styles.container}>
        <WelcomePage />
      </View>
    );
  }
  return <AppScreen />;
};
export default App;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  input: {
    paddingRight: 100,
    marginBottom: 20,
    borderRadius: 5,
    fontSize: 15,
    backgroundColor: 'gray',
  },
  errorText: {
    fontSize: 15,
    color: 'red',
    alignSelf: 'center',
    marginTop: 10,
  },
  buttonText: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 20,
  },
  buttonContainer: {
    borderRadius: 8,
    backgroundColor: 'red',
    padding: 15,
  },
  topRightButton: {
    
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
  },
  name: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  }
});
