import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import colors from './config/colors';

const ButtonPress = ({ onPress, children }) => {
    return (
        <TouchableOpacity onPress={onPress} style={styles.button}>
            <Text style={styles.text}>{children}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        borderWidth: 1,
        borderColor: colors.primary,
        backgroundColor: colors.secondary,
        padding: 15,
        width: '100%',
    },

    text: {
        textAlign: 'center',
        color: colors.text,
        fontWeight: 'bold',
        fontSize: 20,
    },
});

export { ButtonPress };
