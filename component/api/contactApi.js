import { firebase } from '@react-native-firebase/firestore'
import auth from '@react-native-firebase/auth';

export function createContact(contact, createComplete){
    firebase.firestore()
        .collection('users')
        .doc(auth().currentUser.uid)
        .collection('contact')
        .add({
            fullName: contact.fullName,
            email: contact.email,
            pNumber: contact.pNumber,
            createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        }).then((snapshot) => snapshot.get())
        .then((contactData) => createComplete(contactData.data()))
        .catch((error) => console.log(error));
}

export async function getContact(contactRetrieved){
    
    var contactList = [];
    
    var snapshot = await firebase.firestore()
        .collection('users')
        .doc(auth().currentUser.uid)
        .collection('contact')
        .orderBy('name', 'asc')
        .get()

        snapshot.forEach((doc) => {
            contactList.push(doc.data());
        });
        contactRetrieved(contactList);
}