import React, { Component } from 'react';
import {SafeAreaView, StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import colors from '../config/colors';
import {ButtonOption} from '../ButtonOption';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import MedicationsScreen from './moreTab/MedicationsStack';
import AppointmentScreen from './moreTab/AppointmentScreen';
import PrescriptionsScreen from './moreTab/PrescriptionsScreen';
import ReportScreen from './moreTab/ReportScreen';
import SettingScreen from './moreTab/SettingScreen';
import HelpScreen from './moreTab/HelpScreen';
import AboutUsScreen from './moreTab/AboutUsScreen';
import auth,{firebase} from '@react-native-firebase/auth';
import Profile from './moreTab/ProfileScreen';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { enableScreens } from 'react-native-screens';

enableScreens();

class More extends Component{
  render(){
    return (
      <SafeAreaView style={styles.container}>
        <ButtonOption onPress={() => this.props.navigation.navigate('Medications')}>Medications</ButtonOption>
        <ButtonOption onPress={() => this.props.navigation.navigate('Appointment')}>Appointment</ButtonOption>
        <ButtonOption onPress={() => this.props.navigation.navigate('Prescriptions')}>Prescriptions</ButtonOption>
        <ButtonOption onPress={() => this.props.navigation.navigate('Report')}>Report</ButtonOption>
        <ButtonOption onPress={() => this.props.navigation.navigate('Settings')}>Settings</ButtonOption>
        <ButtonOption onPress={() => this.props.navigation.navigate('Help')}>Help & Support</ButtonOption>
        <ButtonOption onPress={() => this.props.navigation.navigate('AboutUs')}>About Us</ButtonOption>
      </SafeAreaView>
    );
  }
}

const Stack = createNativeStackNavigator();

class MoreStack extends Component {
  constructor(props){
    super(props);
    this.state = {
      user: {
        fName: '',
        lName: '',
      }
    }
  }
  
  db = firebase.firestore().collection('users').doc(auth().currentUser.uid);
  render() {
    const db = firebase.firestore();
    const fullName = db.collection('users')
      .doc(auth().currentUser.uid)
      .onSnapshot(documentSnapshot => {
        this.setState({
          user: {
            fName: documentSnapshot.data().fullName.fName,
            lName: documentSnapshot.data().fullName.lName,
          }
        })
      });
  return (
    <Stack.Navigator initialRouteName="More"
      screenOptions={{
        headerStyle: {
          backgroundColor: colors.text,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 20,
        },
      }}>
      <Stack.Screen 
        name="More"  
        component={More} 
        options={{
          title: 'More',
          headerRight: () => (
            <TouchableOpacity style={styles.topRightButton} onPress={() => this.props.navigation.navigate('Profile')}>
              <Text style={styles.name}>{this.state.user.fName} {this.state.user.lName}  </Text>
              <MaterialIcons name="account-circle" size={28} color={'white'} />
            </TouchableOpacity>
          ),
        }}
      />
      <Stack.Screen options={{headerShown: false}} name="Medications" component={MedicationsScreen} />
      <Stack.Screen options={{headerShown: false}} name="Appointment" component={AppointmentScreen} />
      <Stack.Screen options={{headerShown: false}} name="Prescriptions" component={PrescriptionsScreen} />
      <Stack.Screen name="Report" component={ReportScreen} />
      <Stack.Screen name="Settings" component={SettingScreen} />
      <Stack.Screen name="Help" component={HelpScreen} />
      <Stack.Screen name="AboutUs" component={AboutUsScreen} />
      <Stack.Screen name="Profile" component={Profile} />
    </Stack.Navigator>
  );
}
}
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    backgroundColor: colors.primary,
  },
  topRightButton: {

    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
  },
  name: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
  topRightButton: {
      
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
  },
});
export default MoreStack;