import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import {hook, wrap} from 'cavy';
import firestore from '@react-native-firebase/firestore';
import colors from './config/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

GoogleSignin.configure({
  webClientId:
    '78045108977-atpb9is5g7kuku18kvijkl440eg2rs43.apps.googleusercontent.com',
});
class EmailAndPassword extends Component {
  state = {
    email: '',
    password: '',
    isLoading: false,
    errorMessage: null,
  };

  onBottomPress = () => {
    if (this.state.email === '' && this.state.password === '') {
      Alert.alert('Enter details to signin!');
    } else {
      this.setState({
        isLoading: true,
      });
      auth()
        .signInWithEmailAndPassword(this.state.email, this.state.password)
        .then(this.onLoginSuccess)
        .catch((error) => this.setState({errorMessage: error.message}));
    }
  };
  onGoogleButtonPress = async () => {
    // Get the users ID token
    const {idToken} = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  };
  onLoginSuccess = () => {
    this.setState({
      error: '',
      loading: false,
    });
  };
  render() {
    const TestableText = wrap(TextInput);
    const TestableButton = wrap(TouchableOpacity);
    if (this.state.isLoading) {
      return (
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E" />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.errorMessage}>
          {this.state.errorMessage && (
            <Text style={styles.error}>{this.state.errorMessage}</Text>
          )}
        </View>
        <View style={styles.bar}>
          <MaterialIcons name="email" size={24} color={colors.text} />
          <TestableText
            ref={this.props.generateTestHook('EmailAndPassword.InputEmail')}
            placeholder="Email"
            style={styles.input}
            value={this.state.email}
            onChangeText={(email) => this.setState({email})}
          />
        </View>
        <View style={styles.bar}>
          <MaterialIcons name="lock" size={24} color={colors.text} />
          <TestableText
            ref={this.props.generateTestHook('EmailAndPassword.InputPassword')}
            placeholder="Password"
            style={styles.input}
            value={this.state.password}
            onChangeText={(password) => this.setState({password})}
            secureTextEntry
          />
        </View>

        <TestableButton
          ref={this.props.generateTestHook('EmailAndPassword.ButtonLogIn')}
          style={styles.LogInContainer}
          onPress={this.onBottomPress}>
          <Text style={styles.buttonText}>Log In</Text>
        </TestableButton>

        <View>
          <GoogleSigninButton
            style={{width: 192, height: 58}}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            onPress={this.onGoogleButtonPress}
          />
        </View>

        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('SignUpForm')}>
          <Text style={styles.noAccount}>Don't have an account?</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F1F9FF',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  input: {
    borderBottomWidth: 2,
    borderColor: colors.secondary,
    marginBottom: 20,
    marginLeft: 10,
    fontSize: 15,
    width: '70%',
  },
  error: {
    fontSize: 15,
    color: 'red',
    fontWeight: '600',
    alignSelf: 'center',
    marginTop: 10,
  },
  errorMessage: {
    height: 72,
    justifyContent: 'center',
    alignSelf: 'center',
    marginHorizontal: 30,
  },
  buttonText: {
    textAlign: 'center',
    color: '#2699FB',
    fontWeight: 'bold',
    fontSize: 20,
  },
  LogInContainer: {
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#BCE0FD',
    padding: 15,
    bottom: 3,
  },
  GoogleContainer: {
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#BCE0FD',
    padding: 15,
  },
  noAccount: {
    color: '#2699FB',
    fontWeight: 'bold',
    bottom: 5,
    textAlign: 'center',
  },
  bar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  
});
const TestableScene = hook(EmailAndPassword);
export default TestableScene;
