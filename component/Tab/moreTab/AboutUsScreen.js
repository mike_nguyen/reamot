import React, {Component} from 'react';
import {SafeAreaView, Image, View, Text, StyleSheet} from 'react-native';
import colors from '../../config/colors';

class AboutUs extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Image
            style={styles.logo}
            source={require('../../img/aerion-logo-white-text.png')}
          />
        <View style={styles.textContainer}>
          <Text style={styles.text}>
            Aerion Technologies provides Software Consultancy and Development Services for enterprise.
          </Text>
          <Text style={styles.text}>
            We help convert your ideas into software solutions that drive efficiency gains, add value to your customers and differentiate you from your competitors.
          </Text>
          <Text style={styles.text}>
            Our team are passionate about the journey from idea to software, taking concepts from their infancy stage to delivering tangible, valuable and measurable solutions.
          </Text>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.text,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 285,
    height: 56,
  },
  textContainer: {
    width: "80%",
  },
  text: {
    textAlign: 'center',
    marginTop: 20,
    color: 'white'
  },
});
export default AboutUs;