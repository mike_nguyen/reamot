import {createStackNavigator} from '@react-navigation/stack';
import * as React from 'react';
import {Component} from 'react';
import {hook, wrap} from 'cavy';
import Profile from './ProfileScreen';
import ProfileUpdate from './profileUpdate';

const Stack = createStackNavigator();
class profileNavigation extends Component {
  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#96E4DC',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerTitleAlign: 'center',
          headerShown: false,
        }}>
        <Stack.Screen name="profile" component={Profile} />
        <Stack.Screen name="profileUpdate" component={ProfileUpdate} />
      </Stack.Navigator>
    );
  }
}
export default profileNavigation;
