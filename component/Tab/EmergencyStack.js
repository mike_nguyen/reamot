import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import addContact from './add/addContact';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import colors from '../config/colors';
import { enableScreens } from 'react-native-screens';
import Emergency from '../Tab/screen/EmergencyScreen'


enableScreens();

const Stack = createNativeStackNavigator();

class EmergencyStack extends Component {
  render() {
    return (
      <Stack.Navigator 
        screenOptions={{
        headerStyle: {
          backgroundColor: colors.text,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 20,
        },
      }}>
        
        <Stack.Screen 
          name="Emergency Contact"
          component={Emergency}
          options={{
            title: 'Emergency Contact',
            headerRight: () => (
              <TouchableOpacity 
                style={styles.topRightButton} 
                onPress={() => this.props.navigation.navigate('add Emergency Contact')}>
                <MaterialIcons name="add" size={24} color={'white'} />
              </TouchableOpacity>
            ),
          }}
        />
        <Stack.Screen 
          name="add Emergency Contact" 
          component={addContact} 
          options={{
            title: 'add Emergency Contact',
            headerRight: () => (
              <TouchableOpacity style={styles.topRightButton}>
                <MaterialIcons name="check" size={24} color={'white'} />
              </TouchableOpacity>
            ),
          }}/>
          <Stack.Screen 
            name="addContact" 
            component={addContact}
            options={{
              title: 'add Emergency Contact'
            }} />
      </Stack.Navigator>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  topRightButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
  },
});
export default EmergencyStack;
