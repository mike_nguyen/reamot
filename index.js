/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './component/App';
import {name as appName} from './app.json';
import AppWrap from './component/AppWrap';
AppRegistry.registerComponent(appName, () => AppWrap);
