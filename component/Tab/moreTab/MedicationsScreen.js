import React, {Component} from 'react';
import {SafeAreaView, View, Text, Button, StyleSheet} from 'react-native';
import colors from '../../config/colors';

class Medications extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text>Medications</Text>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
});
export default Medications;