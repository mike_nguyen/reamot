import Profile from '../Tab/profileNavigation';
import Setting from '../Tab/SettingScreen';
import LogOut from '../Tab/LogOut';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
class ProfileTab extends Component {
  render() {
    const createProfileStack = () => (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#96E4DC',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerTitleAlign: 'center',
          headerShown: false,
        }}>
        <Stack.Screen name="User" component={Profile} />
      </Stack.Navigator>
    );
    return (
      <Drawer.Navigator drawerPosition="right">
        <Drawer.Screen name="Profile" children={createProfileStack} />
        <Drawer.Screen name="Setting" component={Setting} />
        <Drawer.Screen name="LogOut" component={LogOut} />
      </Drawer.Navigator>
    );
  }
}
export default ProfileTab;
