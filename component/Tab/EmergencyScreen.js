import * as React from 'react';
import {  Component} from "react";
import { createStackNavigator } from '@react-navigation/stack';
import { SafeAreaView, View, StyleSheet } from 'react-native';
import colors from '../config/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
const Stack = createStackNavigator();
class EmergencyTab extends Component {
    render() {
        return (
            <Stack.Navigator
                screenOptions={{
                    headerStyle: {
                        backgroundColor: colors.text,
                    },
                    headerTintColor: 'white',
                    headerTitleStyle: {
                        fontWeight: 'bold',
                        fontSize: 20,
                    },
                }}>
                <Stack.Screen
                    name="Emergency"
                    component={Emergency}
                    options={{
                        title: 'Emergency Contact',
                        headerRight: () => (
                            <View style={styles.topRightButton}>
                                <MaterialIcons name="add" size={24} color={'white'} />
                            </View>
                        ),
                    }}
                />
            </Stack.Navigator>
        );
    }
}
class Emergency extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
    },
    topRightButton: {

        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 10,
    },
});
export default EmergencyTab;