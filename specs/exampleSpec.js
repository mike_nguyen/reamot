export default function(spec) {

  spec.describe('Logging in', function () {
    spec.it('works', async function () {
      await spec.press('WelcomePage.LogInButton');
      await spec.pause(1000);
      await spec.fillIn('EmailAndPassword.InputEmail', 'admin2@test.com');
      await spec.pause(1000);
      await spec.fillIn('EmailAndPassword.InputPassword', '123456');
      await spec.pause(1000);
      await spec.press('EmailAndPassword.ButtonLogIn');
      await spec.pause(10000);
      await spec.press('Home.GotoMore');
      await spec.pause(10000);
      await spec.press('More.GotoProfile');
      await spec.pause(10000);
      await spec.press('LogOut.Button');
    });
  });
}