import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import colors from '../../config/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import auth from '@react-native-firebase/auth';
import { firebase } from '@react-native-firebase/firestore';
import addMedication from '../add/addMedication';
import Medications from '../screen/MedicationScreen'
import { enableScreens } from 'react-native-screens';
enableScreens();

const Stack = createNativeStackNavigator();

class MedicationStack extends Component {
   db = firebase.firestore().collection('users').doc(auth().currentUser.uid).get();
  render(){
    return (
      <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: colors.text,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 20,
        },
      }}>

        <Stack.Screen 
          name="Medications" 
          component={Medications}
          options={{
            title: 'Medications',
            headerRight: () => (
              <TouchableOpacity 
                style={styles.topRightButton} 
                onPress={() => this.props.navigation.navigate('add Medications')}>
                <MaterialIcons name="add" size={24} color={'white'} />
              </TouchableOpacity>
            ),
          }}
        />
        <Stack.Screen 
          name="add Medications" 
          component={addMedication} 
          options={{
            title: 'add Medication',
            headerRight: () => (
              <TouchableOpacity style={styles.topRightButton}>
                <MaterialIcons name="check" size={24} color={'white'} />
              </TouchableOpacity>
            ),
        }}/>
        <Stack.Screen 
          name="addMedication" 
          component={addMedication}
          options={{
            title: 'add Medication'
          }} />
      </Stack.Navigator>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  topRightButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
  },
});
export default MedicationStack;