import {
  View,
  Text,
  StyleSheet,
  Image,
  SafeAreaView,
} from 'react-native';
import React, {Component} from 'react';
import Schedule from './calender/Schedule';
import colors from '../config/colors';
import auth, {firebase} from '@react-native-firebase/auth';

class Home extends Component {
  state = {
    user: {
      fName: '',
    },
  };

  render() {
    let {fName} = this.state;
    const db = firebase.firestore();
    fName = db
      .collection('users')
      .doc(auth().currentUser.uid)
      .onSnapshot((documentSnapshot) => {
        this.setState({
          user: {
            fName: documentSnapshot.data().fullName.fName,
          },
        });
      });

    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            source={require('../img/Reamot_logo.png')}
          />
        </View>
        <View style={styles.welcomeContainer}>
          <Text style={styles.welcomeText}>
            Welcome back, {this.state.user.fName}
          </Text>
        </View>

        <View style={styles.nextContainer}>
          <Text style={styles.welcomeText}>Next:</Text>
        </View>

        <View style={styles.calender}>
          <Schedule />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },

  logoContainer: {
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },

  logo: {
    justifyContent: 'flex-start',
    width: 66,
    height: 63.92,
  },

  welcomeContainer: {
    backgroundColor: 'white',
    marginTop: 10,
    flex: 0,
    padding: 10,
    height: 55,
    width: '90%',
    elevation: 5,
  },

  nextContainer: {
    backgroundColor: 'white',
    marginTop: 10,
    flex: 0,
    padding: 10,
    height: 85,
    width: '90%',
    elevation: 5,
  },

  welcomeText: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  calender: {
    marginTop: 10,
    width: '100%',
    flex: 1,
    elevation: 5,
  },
});
export default Home;
