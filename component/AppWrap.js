import {Tester, TestHookStore} from 'cavy';
import cavy from '../specs/exampleSpec';
import MyApp from './App';
import {Component} from 'react';
import * as React from 'react';
const testHookStore = new TestHookStore();

export default class AppWrapper extends Component {
  render() {
    return (
      <Tester specs={[cavy]} store={testHookStore} waitTime={500}>
        <MyApp />
      </Tester>
    );
  }
}
