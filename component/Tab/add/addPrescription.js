import React, {Component} from 'react';
import {SafeAreaView, Text, StyleSheet} from 'react-native';
import colors from '../../config/colors';

class addPrescription extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text>addPrescription</Text>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
});
export default addPrescription;