export default {
  primary: '#F1F9FF',
  secondary: '#BCE0FD',
  text: '#2699FB',
  input: '#D3D3D3',
};
