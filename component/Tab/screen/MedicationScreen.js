import React, {Component} from 'react';
import {SafeAreaView, StyleSheet, FlatList, Text, View, ActivityIndicator} from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import colors from '../../config/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class Medications extends Component {
    state = {
        medications: [],
        loading: true
    }

    constructor(props) {
        super(props);
        this.subscriber = 
          firestore()
          .collection('users')
          .doc(auth().currentUser.uid)
          .collection('medications')
          .onSnapshot(docs => {
            let medications = []
            docs.forEach(doc => {
              medications.push(doc.data())
            })
            this.setState({ medications })
          })
      }

    renderList = post => {
    return (
        <View style={styles.box}>
            {this.state.medications.map((medication,index) => 
              <View key={index} style={{flex: 1, marginLeft: 8}}>
              <Text style={styles.name}>{medication.medName} ({medication.strength} {medication.unit})</Text>
              <Text>Time: {medication.chosenTime}</Text>
              </View>
            )}
        <MaterialIcons name="chevron-right" size={24} color="black" />
        </View>
    );
    };

    render(){
      return(
        <SafeAreaView style={styles.container}>
            <FlatList  
                style={styles.list} 
                data={this.state.medications} 
                renderItem={({item}) => this.renderList(item)}
                keyExtractor={item => item.id}
                showsVerticalScrollIndicator={false}
            />
        </SafeAreaView>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.primary,
    },
    list: {
      marginHorizontal: 16,
    },
    box: {
      backgroundColor: "#FFF",
      padding: 8,
      borderRadius: 5,
      marginVertical: 8,
      elevation: 5,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center"
    },
    name: {
      fontSize: 20,
      fontWeight: "500",
    },
  });
  