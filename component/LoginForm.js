import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  NativeEventEmitter,
  NativeModules,
  Platform,
} from 'react-native';
import auth, {firebase} from '@react-native-firebase/auth';
import {GoogleSignin} from '@react-native-community/google-signin';
import colors from './config/colors';
import {ButtonBlue} from './ButtonBlue';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ReactNativeAN from 'react-native-alarm-notification';
import moment from 'moment';
const {RNAlarmNotification} = NativeModules;
const RNEmitter = new NativeEventEmitter(RNAlarmNotification);
const repeatAlarmNotifData = {
  title: 'Alarm',
  message: 'Stand up',
  vibrate: true,
  play_sound: true,
  has_button: true,
  channel: 'wakeup',
  data: {foo: 'bar'},
  loop_sound: true,
  schedule_type: 'repeat',
  repeat_interval: 'daily',
};
class SignInDetail extends Component {
  state = {
    email: '',
    password: '',
    error: '',
    loading: false,
    fireDate: '',
    alarmId: '',
  };
  _subscribeOpen;
  _subscribeDismiss;
  calculateAlarmTime = (AlarmTime) => {
    const hour = parseInt(AlarmTime.substring(0, 2));
    const minute = parseInt(AlarmTime.substring(3, 5));
    const second = parseInt(AlarmTime.substring(6, 8));
    const morning_afternoon = AlarmTime.substring(9, 11);
    const now = new Date();
    if (morning_afternoon == 'am') {
      if (hour == 12) {
        now.setHours(hour - 12, minute, second);
      } else {
        now.setHours(hour, minute, second);
      }
    } else if (morning_afternoon == 'pm') {
      if (hour == 12) {
        now.setHours(hour, minute, second);
      } else {
        now.setHours(hour + 12, minute, second);
      }
    }
    if (now <= Date.now()) {
      now.setDate(now.getDate() + 1);
    }
    console.log(moment(now).format());
    return now;
  };
  onPressLogIn = () => {
    auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => {
        this.setAlarm();
      })
      .catch((err) => {
        this.setState({
          error: err.message,
        });
      });
  };

  findIdAN = async (alarm_id, DocId) => {
    const alarm = await ReactNativeAN.getScheduledAlarms();
    let idAN = '';
    for (let i = 0; i < alarm.length; i++) {
      //if (alarm[i].alarmId == alarm_id) {
      idAN = alarm[i].id;
      console.log(`idAN: ${idAN}`);
      firebase
        .firestore()
        .collection('users')
        .doc(auth().currentUser.uid)
        .collection('medications')
        .doc(DocId)
        .update({
          idAN: idAN,
          alarmId: alarm_id,
        });
      //}
    }
    // This is having problem because you cannot set a firestore inside a firestore.
  };
  setAlarm = async () => {
    const alarm = await ReactNativeAN.getScheduledAlarms();
    console.log(alarm);
    firebase
      .firestore()
      .collection('users')
      .doc(auth().currentUser.uid)
      .collection('medications')
      .get()
      .then((docs) => {
        docs.forEach((doc) => {
          const alarmID = Math.floor(Math.random() * 10000).toString();
          const AlarmTime = this.calculateAlarmTime(doc.data().chosenTime);
          console.log(moment(AlarmTime).format());
          const details = {
            ...repeatAlarmNotifData,
            // Need to check it.
            fire_date: ReactNativeAN.parseDate(AlarmTime),
            title: doc.data().medName,
            alarm_id: alarmID,
          };
          ReactNativeAN.scheduleAlarm(details);
          this.findIdAN(details.alarm_id, doc.id);
        });
      });
  };

  onGoogleButtonPress = async () => {
    const {idToken} = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  };
  componentDidMount() {
    this._subscribeDismiss = RNEmitter.addListener(
      'OnNotificationDismissed',
      (data) => {
        const obj = JSON.parse(data);
        console.log(`notification id: ${obj.id} dismissed`);
      },
    );

    this._subscribeOpen = RNEmitter.addListener(
      'OnNotificationOpened',
      (data) => {
        console.log(data);
        const obj = JSON.parse(data);
        console.log(`app opened by notification: ${obj.id}`);
      },
    );

    // check ios permissions
    if (Platform.OS === 'ios') {
      this.showPermissions();

      ReactNativeAN.requestPermissions({
        alert: true,
        badge: true,
        sound: true,
      }).then(
        (data) => {
          console.log('RnAlarmNotification.requestPermissions', data);
        },
        (data) => {
          console.log('RnAlarmNotification.requestPermissions failed', data);
        },
      );
    }
  }
  stopAlarmSound = () => {
    ReactNativeAN.stopAlarmSound();
  };
  componentWillUnmount() {
    this._subscribeDismiss.remove();
    this._subscribeOpen.remove();
  }

  showPermissions = () => {
    ReactNativeAN.checkPermissions((permissions) => {
      console.log(permissions);
    });
  };
  render() {
    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            backgroundColor: colors.primary,
          }}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            source={require('./img/Reamot_logo.png')}
          />
        </View>

        <View style={styles.bar}>
          <MaterialIcons name="email" size={24} color={colors.text} />
          <TextInput
            placeholder="Email"
            style={styles.input}
            value={this.state.email}
            onChangeText={(email) => this.setState({email})}
          />
        </View>

        <View style={styles.bar}>
          <MaterialIcons name="lock" size={24} color={colors.text} />
          <TextInput
            placeholder="Password"
            style={styles.input}
            value={this.state.password}
            onChangeText={(password) => this.setState({password})}
            secureTextEntry
          />
        </View>
        <TouchableOpacity>
          <Text style={styles.noAccount}>Don't have an account? Sign Up</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => this.onGoogleButtonPress()}>
          <Image
            style={styles.google}
            source={require('./img/Google_Light.png')}
          />
        </TouchableOpacity>

        <ButtonBlue onPress={() => this.onPressLogIn()}>Log In</ButtonBlue>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  input: {
    borderBottomWidth: 2,
    borderColor: colors.secondary,
    marginBottom: 20,
    marginLeft: 10,
    fontSize: 15,
    width: '70%',
  },

  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },

  logo: {
    justifyContent: 'flex-start',
    width: 287,
    height: 278,
  },

  google: {
    width: 202,
    height: 46,
  },

  noAccount: {
    color: colors.text,
    fontWeight: 'bold',
    bottom: 5,
    textAlign: 'center',
  },

  errorText: {
    fontSize: 15,
    color: 'red',
    alignSelf: 'center',
    marginTop: 10,
  },

  bar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
export default SignInDetail;
