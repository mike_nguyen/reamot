import React from 'react';
import {
  StyleSheet,
  Text,
  Image,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';

import colors from '../component/config/colors';
import {NavigationContainer} from '@react-navigation/native';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import SignUpForm from './SignupForm';
import LoginForm from './LoginForm';
import { enableScreens } from 'react-native-screens';

enableScreens();

function WelcomePage({navigation}) {
  return (
    <SafeAreaView style={styles.container}>
      <SafeAreaView style={styles.logoContainer}>
        <Image style={styles.logo} source={require('./img/Reamot_logo.png')} />
      </SafeAreaView>

      <TouchableOpacity
        style={styles.ButtonContainer}
        onPress={() => navigation.navigate('LoginForm')}>
        <Text style={styles.buttonText}>Log In</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.ButtonContainer}
        onPress={() => navigation.navigate('SignUpForm')}>
        <Text style={styles.buttonText}>Sign Up</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const Stack = createNativeStackNavigator();

function Loginform() {
  return <LoginForm />;
}
function SignUpform() {
  return <SignUpForm />;
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="WelcomePage"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="WelcomePage" component={WelcomePage} />
        <Stack.Screen name="LoginForm" component={Loginform} />
        <Stack.Screen name="SignUpForm" component={SignUpform} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  logo: {
    width: 287,
    height: 278,
  },
  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
  },
  ButtonContainer: {
    borderWidth: 1,
    borderColor: colors.primary,
    backgroundColor: colors.secondary,
    width: '100%',
    padding: 15,
  },
  
  buttonText: {
    textAlign: 'center',
    color: colors.text,
    fontWeight: 'bold',
    fontSize: 20,
  },
});
export default App;
