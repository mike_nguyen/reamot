import React, {Component} from 'react';
import {SafeAreaView, Text, Button, TextInput, View, StyleSheet} from 'react-native';
import colors from '../../config/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import auth from '@react-native-firebase/auth';
import { firebase } from '@react-native-firebase/firestore'
import { createContact } from '../../api/contactApi'

class addContact extends Component {
  state = {
    fullName: '',
    email: '',
    pNumber: '',
    contactList: [],
  };

  onContactAdded = (contact) => {
    this.setState(prevState => ({
      contactList: [...prevState.contactList, contact]
    }));
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.bar}>
          <MaterialIcons name="account-box" size={24} color={colors.text} />
          <TextInput
            placeholder="Name"
            style={styles.input}
            value={this.state.fullName}
            autoCapitalize="words"
            onChangeText={(fullName) => this.setState({fullName})}
          />
        </View>
        <View style={styles.bar}>
          <MaterialIcons name="phone" size={24} color={colors.text} />
          <TextInput
            placeholder="Phone Number"
            keyboardType="phone-pad"
            style={styles.input}
            value={this.state.pNumber}
            onChangeText={(pNumber) => this.setState({pNumber})}
          />
        </View>
        <View style={styles.bar}>
          <MaterialIcons name="email" size={24} color={colors.text} />
          <TextInput
            placeholder="Email"
            keyboardType="email-address"
            style={styles.input}
            value={this.state.email}
            onChangeText={(email) => this.setState({email})}
          />
        </View>
        <Button 
          title='create Contact' 
          onPress={() =>
            createContact(
              {
                fullName: this.state.fullName,
                email: this.state.email,
                pNumber: this.state.pNumber,
              },
            this.onContactAdded  
            )
          }
        />

      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    alignItems: 'center',
  },
  input: {
    borderBottomWidth: 2,
    borderColor: colors.secondary,
    marginLeft: 10,
    marginBottom: 20,
    borderRadius: 5,
    fontSize: 15,
    width: '70%',
  },
  bar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
export default addContact;