import {
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import React, { Component } from 'react';
import auth from '@react-native-firebase/auth';
import WelcomePage from '../../WelcomePage';
class LogOut extends Component {
  constructor() {
    super();
    this.state = {
      uid: '',
    };
  }
  onPressSignOut = () => {
    auth()
      .signOut()
      .then(() => {
        <WelcomePage/>
      })
      .catch((error) => this.setState({errorMessage: error.message}));
  };
  render() {
    this.state = {
        displayName: auth().currentUser.displayName,
        uid: auth().currentUser.uid,
      };    
    const TestableButton = wrap(TouchableOpacity);
    return (
      <TestableButton
        // ref={this.props.generateTestHook('LogOut.Button')}
        style={styles.buttonContainer}
        onPress={this.onPressSignOut}>
        <Text style={styles.buttonText}>Logout</Text>
      </TestableButton>
    );
  }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    input: {
        paddingRight: 100,
        marginBottom: 20,
        borderRadius: 5,
        fontSize: 15,
        backgroundColor: 'gray',
    },
    errorText: {
        fontSize: 15,
        color: 'red',
        alignSelf: 'center',
        marginTop: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 20,
    },
    buttonContainer: {
        borderRadius: 8,
        backgroundColor: 'blue',
        padding: 15,
    },
});
// const TestableScene = hook(LogOut);
export default LogOut;