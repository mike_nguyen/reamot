import React, {Component} from 'react';
import {SafeAreaView, View, Text, Button, StyleSheet, TouchableOpacity} from 'react-native';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import colors from '../../config/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import auth from '@react-native-firebase/auth';
import { firebase } from '@react-native-firebase/firestore';
import addAppointment from '../add/addAppointment';
import { enableScreens } from 'react-native-screens';

enableScreens();

class Appointment extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>

      </SafeAreaView>
    );
  }
}

const Stack = createNativeStackNavigator();

class AppointmentStack extends Component {
  db = firebase.firestore().collection('users').doc(auth().currentUser.uid);
  render(){
    return (
      <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: colors.text,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 20,
        },
      }}>

        <Stack.Screen 
          name="Appointment" 
          component={Appointment}
          options={{
            title: 'Appointment',
            headerRight: () => (
              <TouchableOpacity 
                style={styles.topRightButton} 
                onPress={() => this.props.navigation.navigate('add Appointment')}>
                <MaterialIcons name="add" size={24} color={'white'} />
              </TouchableOpacity>
            ),
          }}
        />
        <Stack.Screen 
          name="add Appointment" 
          component={addAppointment} 
          options={{
            title: 'add Appointment',
            headerRight: () => (
              <TouchableOpacity style={styles.topRightButton}>
                <MaterialIcons name="check" size={24} color={'white'} />
              </TouchableOpacity>
            ),
        }}/>
        <Stack.Screen 
          name="addAppointment" 
          component={addAppointment}
          options={{
            title: 'add Appointment'
          }} />
      </Stack.Navigator>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  topRightButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
  },
});
export default AppointmentStack;