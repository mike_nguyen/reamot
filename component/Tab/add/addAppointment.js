import React, {Component} from 'react';
import {SafeAreaView, Text, StyleSheet} from 'react-native';
import colors from '../../config/colors';

class addAppointment extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text>addAppointment</Text>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
});
export default addAppointment;