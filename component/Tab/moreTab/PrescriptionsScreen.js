import React, {Component} from 'react';
import {SafeAreaView, View, Text, Button, StyleSheet, TouchableOpacity} from 'react-native';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import colors from '../../config/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import auth from '@react-native-firebase/auth';
import { firebase } from '@react-native-firebase/firestore';
import addPrescription from '../add/addPrescription';
import { enableScreens } from 'react-native-screens';

enableScreens();

class Prescriptions extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>

      </SafeAreaView>
    );
  }
}

const Stack = createNativeStackNavigator();

class PrescriptionStack extends Component {
  db = firebase.firestore().collection('users').doc(auth().currentUser.uid);
  render(){
    return (
      <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: colors.text,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 20,
        },
      }}>

        <Stack.Screen 
          name="Prescription" 
          component={Prescriptions}
          options={{
            title: 'Prescription',
            headerRight: () => (
              <TouchableOpacity 
                style={styles.topRightButton} 
                onPress={() => this.props.navigation.navigate('add Prescriptions')}>
                <MaterialIcons name="add" size={24} color={'white'} />
              </TouchableOpacity>
            ),
          }}
        />
        <Stack.Screen 
          name="add Prescriptions" 
          component={addPrescription} 
          options={{
            title: 'add Prescription',
            headerRight: () => (
              <TouchableOpacity style={styles.topRightButton}>
                <MaterialIcons name="check" size={24} color={'white'} />
              </TouchableOpacity>
            ),
        }}/>
        <Stack.Screen 
          name="addPrescription" 
          component={addPrescription}
          options={{
            title: 'add Prescription'
          }} />
      </Stack.Navigator>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  topRightButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
  },
});
export default PrescriptionStack;