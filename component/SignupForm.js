import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';

import auth, {firebase} from '@react-native-firebase/auth';
import colors from './config/colors';
import {ButtonBlue} from './ButtonBlue';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

class SignUpDetail extends Component {
  state = {
    fullName: {
      fName: '',
      lName: '',
    },
    dob: {
      day: '',
      month: '',
      year: '',
    },
    pNumber: '',
    email: '',
    password: '',
    confirmPassword: '',
    error: '',
    loading: false,
  };
  
  docData = {
    fullName: {
      fName: this.state.fName,
      lName: this.state.lName,
    },
    email: this.state.email,
    dob: {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year,
    },
    pNumber: this.state.pNumber,
    role: 0,
  }
  
  //create user
  createUser = () => {
    auth()
      .createUserWithEmailAndPassword(`${this.state.email}`, `${this.state.password}`)
      .then(cred => {
        var db = firebase.firestore();
        db.collection('users')
          .doc(`${cred.user.uid}`)
          .set(
            {
              fullName: {
                fName: this.state.fName,
                lName: this.state.lName,
              },
              email: this.state.email,
              dob: {
                day: this.state.day,
                month: this.state.month,
                year: this.state.year,
              },
              pNumber: this.state.pNumber,
              role: 0,
              createdAt: firebase.firestore.FieldValue.serverTimestamp()
            
          })
      })
      .catch((error) => {
        if (error.code === 'auth/email-already-in-use') {
          alert('The email address is already in use!');
        }
        if (error.code === 'auth/invalid-email') {
          alert('The email address is invalid!');
        }
      });
  };

  onSignUpSuccess = () => {
    this.setState({
      error: '',
      loading: false,
    });
  };

  //Get UID
  getUID = () => {
    admin
      .auth()
      .getUser(uid)
      .then(function (userRecord) {
        // See the UserRecord reference doc for the contents of userRecord.
      })
      .catch(function (error) {
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            source={require('./img/Reamot_logo.png')}
          />
        </View>

        <Text style={styles.heading}>Personal Information</Text>

        <View style={styles.bar}>
          <MaterialIcons name="account-box" size={24} color={colors.text} />
          <TextInput
            placeholder="First Name"
            style={styles.inputName}
            value={this.state.fName}
            autoCapitalize="words"
            onChangeText={(fName) => this.setState({fName})}
          />
          <TextInput
            placeholder="Last Name"
            style={styles.inputName}
            value={this.state.lName}
            autoCapitalize="words"
            onChangeText={(lName) => this.setState({lName})}
          />
        </View>

        <View style={styles.bar}>
          <MaterialIcons name="date-range" size={24} color={colors.text} />
          <TextInput
            placeholder="Month"
            style={styles.dob}
            value={this.state.month}
            onChangeText={(month) => this.setState({month})}
          />
          <TextInput
            placeholder="Day"
            style={styles.dob}
            value={this.state.day}
            onChangeText={(day) => this.setState({day})}
          />
          <TextInput
            placeholder="Year"
            style={styles.dob}
            value={this.state.year}
            onChangeText={(year) => this.setState({year})}
          />
        </View>

        <View style={styles.bar}>
          <MaterialIcons name="phone" size={24} color={colors.text} />
          <TextInput
            placeholder="Phone Number"
            keyboardType="phone-pad"
            style={styles.input}
            value={this.state.pNumber}
            onChangeText={(pNumber) => this.setState({pNumber})}
          />
        </View>

        <Text style={styles.heading}>Account Information</Text>

        <View style={styles.bar}>
          <MaterialIcons name="email" size={24} color={colors.text} />
          <TextInput
            placeholder="Email"
            keyboardType="email-address"
            style={styles.input}
            value={this.state.email}
            onChangeText={(email) => this.setState({email})}
          />
        </View>

        <View style={styles.bar}>
          <MaterialIcons name="lock" size={24} color={colors.text} />
          <TextInput
            placeholder="Password"
            style={styles.input}
            value={this.state.password}
            onChangeText={(password) => this.setState({password})}
            secureTextEntry
          />
        </View>

        <View style={styles.bar}>
          <MaterialIcons name="lock" size={24} color={colors.text} />
          <TextInput
            placeholder="Re-enter Password"
            style={styles.input}
            value={this.state.confirmPassword}
            onChangeText={(confirmPassword) => this.setState({confirmPassword})}
            secureTextEntry
          />
        </View>

        <TouchableOpacity onPress={() => navigation.navigate('LoginForm')}>
          <Text style={styles.haveAccount}>Have an account?</Text>
        </TouchableOpacity>

        <ButtonBlue onPress={this.createUser}>Sign Up</ButtonBlue>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  input: {
    borderBottomWidth: 2,
    borderColor: colors.secondary,
    marginBottom: 20,
    borderRadius: 5,
    fontSize: 15,
    width: '70%',
  },

  inputName: {
    borderBottomWidth: 2,
    borderColor: colors.secondary,
    marginBottom: 20,
    borderRadius: 5,
    fontSize: 15,
    width: '35%',
  },

  dob: {
    borderBottomWidth: 2,
    borderColor: colors.secondary,
    marginBottom: 20,
    borderRadius: 5,
    fontSize: 15,
    width: '23.3333%',
  },

  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
  },

  logo: {
    justifyContent: 'flex-start',
    width: 66,
    height: 63.92,
  },

  heading: {
    color: colors.text,
    fontWeight: 'bold',
    fontSize: 20,
  },

  haveAccount: {
    color: colors.text,
    fontWeight: 'bold',
    bottom: 5,
    textAlign: 'center',
  },

  bar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
export default SignUpDetail;