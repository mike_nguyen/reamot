/* eslint-disable prettier/prettier */
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Button,
} from 'react-native';
import React, {Component} from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import colors from '../../config/colors';
import auth, { firebase } from '@react-native-firebase/auth';
import ReactNativeAN from 'react-native-alarm-notification';

class Profile extends Component {
  onPressSignOut = () => {
    this.deleteReminders();
    auth().signOut();
  };
  state = {
    user: {
      fName: '',
      lName: '',
    },
  };
  deleteReminders = async () => {
    firebase.firestore()
      .collection('users')
      .doc(auth().currentUser.uid)
      .collection('medications')
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((documentSnapshot) => {
          if (documentSnapshot.data().idAN != '') {
            const id = parseInt(documentSnapshot.data().idAN, 10);
            ReactNativeAN.deleteAlarm(id);
          }
        });
      });
  };
  render() {
    const db = firebase.firestore();
    const fullName = db
      .collection('users')
      .doc(auth().currentUser.uid)
      .onSnapshot((documentSnapshot) => {
        this.setState({
          user: {
            fName: documentSnapshot.data().fullName.fName,
            lName: documentSnapshot.data().fullName.lName,
          },
        });
      });
    return (
      <SafeAreaView style={styles.container}>
        <View style={{alignSelf: 'center'}}>
          <MaterialIcons
            name={'account-circle'}
            style={[{color: 'black'}]}
            size={100}
          />
        </View>

        <View style={styles.nameContainer}>
          <Text style={styles.largeText}>
            {this.state.user.fName} {this.state.user.lName}
          </Text>
        </View>

        <View style={styles.infoContainer}>
          <Text style={styles.smallText}>Gender</Text>
        </View>
        <View style={styles.logOutContainer}>
          <Button onPress={this.onPressSignOut} title="Log Out" color="red" />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  nameContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 16,
  },
  infoContainer: {
    borderBottomWidth: 1,
    borderBottomColor: 'lightgrey',
    padding: 15,
    width: '80%',
    alignItems: 'center',
  },
  largeText: {
    fontWeight: '200', 
    fontSize: 36,
  },
  smallText: {
    textAlign: 'left',
    fontWeight: '200', 
    fontSize: 30,
  },
  logOutContainer: {
    padding: 15,
    width: '100%',
    position: 'absolute',
    bottom: 0,
  }
});

export default Profile;
